## Rock n Roll AI

But général :
Dataset :
- Sampler differents riff de guitares d'acdc 
- nos propres riff
Normalisation : 10s, format, tempo, titre ?
Outliers values
Instruments séparés ? 

Démarche :
- Visualisation : trouver des corrélations entre différents morceaux, retrouver les différents albums ? 
- Clustering : identifier la proximité sonore d'un nouveau riff par apprentissage  supervisé (ie retrouver les albums ?) 
- Génération de nouveaux riffs
- Travail autour de la génération de tablatures ?
- en fonction des 500 titres connu, on entre un nouveau titre (devil mes couilles) et ça génère le riff ? 


```mermaid
flowchart TB
    subgraph sample
        acdc-.-custom
    end

    subgraph matplotlib
        visualisation-.-correlation
    end

    subgraph models
        clustering-.-generation
    end

    subgraph output
        regroup_by_album-.-new_riff
    end
    sample --> matplotlib --> models
    clustering --> regroup_by_album
    generation --> new_riff
```
## Sources

```shell
https://amadeuscode.com/app/en
https://openai.com/blog/musenet/
https://boomy.com/
https://soundraw.io/
https://ecrettmusic.com/
https://www.aiva.ai/
https://www.ampermusic.com/
https://github.com/aishoot/AI_Music_Composition
```




